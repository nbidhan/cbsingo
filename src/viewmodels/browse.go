package viewmodels

import (

)

type Browse struct{
		Title string
		Active string
	}

func GetBrowse() Browse{
		result := Browse{
				Title: "CourseBookStore.com - Browse books",
				Active: "browse",
			}
		return result
	}