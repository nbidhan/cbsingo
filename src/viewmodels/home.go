package viewmodels

import (

)

type Home struct {
	Title string
	Active string
}

func GetHome() Home {
	result := Home{
		Title: "CourseBookStore.com - Buy and sell second hand books for specific courses from fellow students",
		Active: "",
	}
	
	return result
}
