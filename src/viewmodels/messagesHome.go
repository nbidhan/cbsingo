package viewmodels

import (

)

type MessagesHome struct{
		Title string
		Active string
	}

func GetMessagesHome() MessagesHome{
		result := MessagesHome{
				Title: "CourseBookStore.com - << Bidhan Neupane >>'s messages",
				Active: "messages-home",
			}
		return result
	}