package viewmodels

import (

)

type BooksForSale struct{
		Title string
		Active string
	}

func GetBooksForSale() BooksForSale{
		result := BooksForSale{
				Title: "CourseBookStore.com - << Bidhan Neupane >>'s books for sale",
				Active: "books-for-sale",
			}
		return result
	}