package viewmodels

import (

)

type Feedback struct{
		Title string
		Active string
	}

func GetFeedback() Feedback{
		result := Feedback{
				Title: "CourseBookStore.com - << Bidhan Neupane >>'s feedbacks",
				Active: "feedback",
			}
		return result
	}