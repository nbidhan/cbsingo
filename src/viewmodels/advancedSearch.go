package viewmodels

import (

)

type AdvancedSearch struct{
		Title string
		Active string
	}

func GetAdvancedSearch() AdvancedSearch{
		result := AdvancedSearch{
				Title: "CourseBookStore.com - Advanced search",
				Active: "advanced-search",
			}
		
		return result
	}