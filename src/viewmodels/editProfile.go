package viewmodels

import (

)

type EditProfile struct{
		Title string
		Active string
	}

func GetEditProfile() EditProfile{
		result := EditProfile{
				Title: "Bidhan Neupane - Edit profile", // get username from model
				Active: "edit-profile",
			}
		return result
	}