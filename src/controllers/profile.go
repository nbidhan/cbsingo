package controllers

import (
	"net/http"
	"viewmodels"
	"text/template"
	"controllers/util"
)

type profileController struct{
		template *template.Template
	}

func (this *profileController) get(w http.ResponseWriter, req *http.Request){
		vm := viewmodels.GetProfile()
		
		w.Header().Add("Content Type", "text/html")
		responseWriter := util.GetResponseWriter(w, req)
		defer responseWriter.Close()
		
		this.template.Execute(responseWriter, vm)
	}