package controllers

import (
	"net/http"
	"viewmodels"
	"text/template"
	"controllers/util"
)

type editProfileController struct{
		template *template.Template
	}

func (this *editProfileController) get(w http.ResponseWriter, req *http.Request){
		vm := viewmodels.GetEditProfile()
		
		w.Header().Add("Content Type", "text/html")
		responseWriter := util.GetResponseWriter(w,req)
		defer responseWriter.Close()
		
		this.template.Execute(responseWriter, vm)
	}