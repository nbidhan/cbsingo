package controllers

import (
	"net/http"
	"viewmodels"
	"text/template"
	"controllers/util"
)

type booksForSaleController struct{
		template *template.Template
	}

func (this *booksForSaleController) get(w http.ResponseWriter, req *http.Request){
		vm := viewmodels.GetBooksForSale()
		
		w.Header().Add("Content Type", "text/html")
		responseWriter := util.GetResponseWriter(w,req)
		defer responseWriter.Close()
		
		this.template.Execute(responseWriter, vm)
	}