package controllers

import (
  "net/http"
  "os"
  "text/template"
  "bufio"
  "strings"
  "github.com/gorilla/mux"
)

func Register(templates *template.Template) {
	
	router := mux.NewRouter()
	
	hc := new(homeController)
	hc.template = templates.Lookup("index.html")
	router.HandleFunc("/", hc.get)
	
	bc := new(browseController)
	bc.template = templates.Lookup("browse.html")
	router.HandleFunc("/browse", bc.get)
	
	advsc := new(advancedSearchController)
	advsc.template = templates.Lookup("advanced_search.html")
	router.HandleFunc("/advanced-search", advsc.get)
	
	epc := new(editProfileController)
	epc.template = templates.Lookup("edit_profile.html")
	router.HandleFunc("/edit-profile", epc.get)
	
	pc := new(profileController)
	pc.template = templates.Lookup("profile.html")
	router.HandleFunc("/profile", pc.get)
	
	bfsc := new(booksForSaleController)
	bfsc.template = templates.Lookup("books_for_sale.html")
	router.HandleFunc("/books-for-sale", bfsc.get)
	
	mhc := new(messagesHomeController)
	mhc.template = templates.Lookup("messages_home.html")
	router.HandleFunc("/messages-home", mhc.get)
	
	fc := new(feedbackController)
	fc.template = templates.Lookup("feedback.html")
	router.HandleFunc("/feedback", fc.get)
		
	http.Handle("/", router)
	
	http.HandleFunc("/images/", serveResource)
	http.HandleFunc("/css/", serveResource)
	http.HandleFunc("/js/", serveResource)
}

func serveResource(w http.ResponseWriter, req *http.Request) {
	path := "public" + req.URL.Path
	var contentType string
	if strings.HasSuffix(path, ".css") {
		contentType = "text/css"
	} else if strings.HasSuffix(path, ".png") {
		contentType = "image/png"
	} else if strings.HasSuffix(path, ".js") {
		contentType = "application/javascript"
	} else {
		contentType = "text/plain"
	}
	
	f, err := os.Open(path)
	
	if err == nil {
		defer f.Close()
		w.Header().Add("Content Type", contentType)
		
		br := bufio.NewReader(f)
		br.WriteTo(w)
	} else {
		w.WriteHeader(404)
	}
}