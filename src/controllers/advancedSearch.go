package controllers

import (
	"net/http"
	"viewmodels"
	"text/template"
	"controllers/util"
)

type advancedSearchController struct{
		template *template.Template
	}

func (this *advancedSearchController) get(w http.ResponseWriter, req *http.Request){
		vm := viewmodels.GetAdvancedSearch()
		
		w.Header().Add("Content Type", "text/html")
		responseWriter := util.GetResponseWriter(w,req)
		defer responseWriter.Close()
		
		this.template.Execute(responseWriter, vm)
	}