package controllers

import (
	"net/http"
	"viewmodels"
	"text/template"
	"controllers/util"
)

type feedbackController struct{
		template *template.Template
	}

func (this *feedbackController) get(w http.ResponseWriter, req *http.Request){
		vm := viewmodels.GetFeedback()
		
		w.Header().Add("Content Type", "text/html")
		responseWriter := util.GetResponseWriter(w, req)
		defer responseWriter.Close()
		
		this.template.Execute(responseWriter, vm)
	}