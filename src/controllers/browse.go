package controllers

import (
	"net/http"
	"viewmodels"
	"text/template"
	"controllers/util"
)

type browseController struct{
		template *template.Template
	}

func (this *browseController) get(w http.ResponseWriter, req *http.Request){
		vm := viewmodels.GetBrowse()
		
		w.Header().Add("Content Type", "text/html")
		responseWriter := util.GetResponseWriter(w,req)
		defer responseWriter.Close()
		
		this.template.Execute(responseWriter, vm)
		
	}